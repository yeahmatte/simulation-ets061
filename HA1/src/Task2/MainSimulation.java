package Task2;

import java.io.IOException;


public class MainSimulation extends GlobalSimulation{
 
    public static void main(String[] args) throws IOException {
    	
    	Event actEvent;
    	State actState = new State(); // The state that shoud be used
    	// Some events must be put in the event list at the beginning
        insertEvent(QUEUE_A_ARRIVAL, 0);  
        insertEvent(MEASURE, 0.1);
        
        // The main simulation loop
    	while (time < 1000){
    		actEvent = eventList.fetchEvent();
    		time = actEvent.eventTime;
    		actState.treatEvent(actEvent);
    	}
    	
    	printData(actState);
    	
    }
    
    
    private static void printData(State state){
    /*	
    public int[] pop = new int[2];
	public int[] accPop = new int[2]; 
	public int nbrOfObs;
	public int rejectionsQueue1;
	public int[] arrivals = new int[2];
	*/
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append("Buffer average population: " + ((double) ((state.accPop[1]+state.accPop[0])/state.nbrOfObs)) + "(acc: "+state.accPop[0] +" "+ state.accPop[1]+" nbrOfObs: "+state.nbrOfObs+")"	+ System.getProperty("line.separator"));
    	sb.append("Queue1 Arr: " + state.arrivals[0] + " Queue 2 Arr: "+state.arrivals[1]+"" );
    	
    	
    	System.out.print( sb.toString() );
    	
    }
}