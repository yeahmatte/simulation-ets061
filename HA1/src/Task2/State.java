package Task2;

import java.util.Random;

class State extends GlobalSimulation{
	
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int[] pop = new int[2];
	public int[] accPop = new int[2]; 
	public int nbrOfObs;
	public int rejectionsQueue1;
	public int[] arrivals = new int[2];
	boolean systemBusy = false;
	private OutPuter outerA = new OutPuter("task2_b_A.txt");
	private OutPuter outerB = new OutPuter("task2_b_B.txt");
	
	
	Random rnd = new Random(); // This is just a random number generator
	
	private double interarrivalQueue1(){
		return (Math.log( 1 - rnd.nextDouble())  * (-0.0066666666666666666666667));
		//
	}
	
	private double interarrivalQueue2(){
		//return 1; 
		return (Math.log( 1 - rnd.nextDouble())  *(-1));
 	}
	
	private double serviceTimeQueue1(){
		return 0.002;
	}
	
	private double serviceTimeQueue2(){
		return 0.004;
	}
	
	private double obsTime(){
		return 0.1;
	}
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case QUEUE_A_ARRIVAL:
				arrival(0);
				break;
			case QUEUE_A_DEPARTURE:
				ready(0);
				break;
			case MEASURE:
				measure();
				break;
			case QUEUE_B_ARRIVAL:
				arrival(1);
				break;
			case QUEUE_B_DEPARTURE:
				ready(1);
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrival(int queue){
		arrivals[queue]++;
		
		if (pop[0] == 0 && pop[1] == 0 && !systemBusy)
			if(queue == 0){
				insertEvent(QUEUE_A_DEPARTURE, time + serviceTimeQueue1());
				systemBusy = true;
			} else {
				insertEvent(QUEUE_B_DEPARTURE, time + serviceTimeQueue2());
				systemBusy = true;
			}
		else
			pop[queue]++;
		
		if(queue == 0)
			insertEvent(QUEUE_A_ARRIVAL, time + interarrivalQueue1());
		//else
		//insertEvent(QUEUE2_ARRIVAL, time + 2.5*slump.nextDouble());
	}
	
	private void ready(int queue){
		systemBusy = false;
		
		//Task2 a and b
		
		if (pop[1] > 0) {
			insertEvent(QUEUE_B_DEPARTURE, time + serviceTimeQueue2());
			systemBusy = true;
			pop[1]--;
		} else if (pop[0] > 0) {
			insertEvent(QUEUE_A_DEPARTURE, time + serviceTimeQueue1());
			systemBusy = true;
			pop[0]--;
		} else {
			//Do nothing
		} /*/ 
		
		//Task2 c
		///*
		if (pop[0] > 0) {
			insertEvent(QUEUE_A_DEPARTURE, time + serviceTimeQueue1());
			systemBusy = true;
			pop[0]--;
		}
		else if (pop[1] > 0) {
				insertEvent(QUEUE_B_DEPARTURE, time + serviceTimeQueue2());
				systemBusy = true;
				pop[1]--;
			}
		 else {
			//Do nothing
		}
		//*/
		
		if (queue == 0) {
			insertEvent(QUEUE_B_ARRIVAL, time + interarrivalQueue2());
		}
	}
	
	
	private void measure(){
		accPop[0] += pop[0];
		accPop[1] += pop[1];
		nbrOfObs++;
		outerA.print(time+";"+pop[0]);
		outerB.print(time+";"+pop[1]);
		
		insertEvent(MEASURE, time +obsTime());
	}
}