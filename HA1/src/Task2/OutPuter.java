package Task2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class OutPuter {
	PrintWriter writer;
	
	public OutPuter(String filename){
		try{
			writer = new PrintWriter("C:\\Users\\yeahmatte\\Documents\\Git\\simulation-ets061\\HA1\\matlab\\"+filename);
			} catch (FileNotFoundException e){
				System.out.println("Error!");
			}
	}
	
	public void print(String msg){
		writer.println(msg);
	}
	
	public void close() {
		writer.close();
	}
}
