package task1;
public class EventListClass {
	
	private Event firstEvent, lastEvent; // Used to build a linked list
	
	EventListClass(){
		firstEvent = new Event();
    	lastEvent = new Event();
    	firstEvent.next = lastEvent;
	}
	
	// The method insertEvent creates a new event, and searches the list of events for the 
	// right place to put the new event.
	
	public void InsertEvent(int type, double TimeOfEvent){
 	Event dummy, predummy;
 	Event newEvent = new Event();
 	newEvent.eventType = type;
 	newEvent.eventTime = TimeOfEvent;
 	predummy = firstEvent;
 	dummy = firstEvent.next;
 	while ((dummy.eventTime < newEvent.eventTime) & (dummy != lastEvent)){
 		predummy = dummy;
 		dummy = dummy.next;
 	}
 	predummy.next = newEvent;
 	newEvent.next = dummy;
 }
	
	
	
	// The following method removes and returns the first event in the list. That is the
	// event with the smallest time stamp, i.e. the next thing that shall take place.
	
	public Event fetchEvent(){
		Event dummy;
		dummy = firstEvent.next;
		firstEvent.next = dummy.next;
		dummy.next = null;
		return dummy;
	}
}