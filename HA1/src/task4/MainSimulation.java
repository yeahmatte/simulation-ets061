package task4;
import java.util.*;
import java.io.*;


public class MainSimulation extends GlobalSimulation{
 
    public static void main(String[] args) throws IOException {
    	
    	OutPuter out = new OutPuter();
    	Event actEvent;
    	State actState = new State(out); // The state that shoud be used
    	// Some events must be put in the event list at the beginning
        insertEvent(ARRIVAL, 0);  
        insertEvent(MEASURE, 0);
        
        // The main simulation loop
    	while (time < 16000){
    		actEvent = eventList.fetchEvent();
    		time = actEvent.eventTime;
    		actState.treatEvent(actEvent);
    	}
    	
    	out.close();
    	printData(actState);
    	
    }
    
    
    private static void printData(State state){

    	System.out.println("Done");
    	
    }
}