package task4;
import java.util.*;
import java.io.*;

class State extends GlobalSimulation {
	
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int pop = 0;
	public int nbrOfObs;	
	public int arrivals = 0;
	Random slump = new Random(); // This is just a random number generator
	OutPuter outter;

	public State(OutPuter outter) {
		this.outter = outter;
	}
	
	private double interarrivalSystem(){
		return (Math.log( 1 - slump.nextDouble())  * (-0.25));
	}
	
	
	private double serviceTimeSystem(){
		return 10;
		//return (Math.log( 1 - slump.nextDouble())  * (-2.1));
	}
	
	private double obsTime(){
		return 4;
	}
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case ARRIVAL:
				arrival();
				break;
			case DEPARTURE:
				ready();
				break;
			case MEASURE:
				measure();
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrival(){
		arrivals++;
		
		if (pop < 100) {
			insertEvent(DEPARTURE, time + serviceTimeSystem());
			pop++;
		}
		
		insertEvent(ARRIVAL, time + interarrivalSystem());
		//else
		//insertEvent(QUEUE2_ARRIVAL, time + 2.5*slump.nextDouble());
	}
	
	private void ready(){
		pop--;
		
	}
	
	
	private void measure(){
		nbrOfObs++;
		
		outter.print(nbrOfObs + ":" + pop );
		
		insertEvent(MEASURE, time +obsTime());
	}
}