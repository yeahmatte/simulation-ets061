package task3;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.io.*;

class State extends GlobalSimulation{
	
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int[] pop = new int[2];
	public int[] accPop = new int[2]; 
	public int nbrOfObs;
	public int rejectionsQueue1;
	public int[] arrivals = new int[2];
	public int[] departures = new int[2];
	Deque<Double> arrivalTimes = new ConcurrentLinkedDeque<Double>();
	double accTimeInSystem = 0;
	
	Random slump = new Random(); // This is just a random number generator
	
	private double interarrivalQueue1(){
		return (Math.log( 1 - slump.nextDouble())  * (-1.1));
	}
	
/*	private int interarrivalQueue2(){
		return 0;
	}*/
	
	private double serviceTimeQueue1(){
		return (Math.log( 1 - slump.nextDouble())  * (-1));
	}
	
	private double serviceTimeQueue2(){
		return (Math.log( 1 - slump.nextDouble())  * (-1));
	}
	
	private double obsTime(){
		return (Math.log( 1 - slump.nextDouble())  * (-2));
	}
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case QUEUE1_ARRIVAL:
				arrival(0);
				break;
			case QUEUE1_DEPARTURE:
				ready(0);
				break;
			case MEASURE:
				measure();
				break;
			case QUEUE2_ARRIVAL:
				arrival(1);
				break;
			case QUEUE2_DEPARTURE:
				ready(1);
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrival(int queue){
		arrivals[queue]++;
		
		if (pop[queue] == 0) {
			if(queue == 0)
				insertEvent(QUEUE1_DEPARTURE, time + serviceTimeQueue1());
			else
				insertEvent(QUEUE2_DEPARTURE, time + serviceTimeQueue2());
		}	
		
		pop[queue]++;
		
		if(queue == 0){
			insertEvent(QUEUE1_ARRIVAL, time + interarrivalQueue1());
			arrivalTimes.add(time);
		}
		//else
		//insertEvent(QUEUE2_ARRIVAL, time + 2.5*slump.nextDouble());
	}
	
	private void ready(int queue){
		pop[queue]--;
		departures[queue]++;
			
		if(queue == 0)
			insertEvent(QUEUE2_ARRIVAL, time);
		else if(queue==1)
			accTimeInSystem += time - arrivalTimes.pop();
			
			
		
		if (pop[queue] > 0){
			if(queue==0) {
				insertEvent(QUEUE1_DEPARTURE, time + serviceTimeQueue1());
			} else {
				insertEvent(QUEUE2_DEPARTURE, time + serviceTimeQueue2());
			}
		}
	}
	
	
	private void measure(){
		accPop[0] += pop[0];
		accPop[1] += pop[1];
		nbrOfObs++;
		
		insertEvent(MEASURE, time +obsTime());
	}
}