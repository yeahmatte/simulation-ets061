package task3;
import java.util.*;
import java.io.*;


public class MainSimulation extends GlobalSimulation{
 
    public static void main(String[] args) throws IOException {
    	
    	Event actEvent;
    	State actState = new State(); // The state that shoud be used
    	// Some events must be put in the event list at the beginning
        insertEvent(QUEUE1_ARRIVAL, 0);  
        insertEvent(MEASURE, 5);
        
        // The main simulation loop
    	while (time < 10000){
    		actEvent = eventList.fetchEvent();
    		time = actEvent.eventTime;
    		actState.treatEvent(actEvent);
    	}
    	
    	printData(actState);
    	
    }
    
    
    private static void printData(State state){
    /*	
    public int[] pop = new int[2];
	public int[] accPop = new int[2]; 
	public int nbrOfObs;
	public int rejectionsQueue1;
	public int[] arrivals = new int[2];
	*/
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append("Queue1 average population: " + ((double) state.accPop[0]/state.nbrOfObs) + "(acc: "+state.accPop[0]+" nbrOfObs: "+state.nbrOfObs+")"	+ System.getProperty("line.separator") );
    	sb.append("Queue2 average population: " + ((double) state.accPop[1]/state.nbrOfObs) + "(acc: "+state.accPop[1]+" nbrOfObs: "+state.nbrOfObs+")"	+ System.getProperty("line.separator") );
    	sb.append("Queue1 arrivals: " + (state.arrivals[0]) + " Queue2 arrivals: " + state.arrivals[1] + System.getProperty("line.separator") );
    	
    	sb.append("Average system population: " + (((double) state.accPop[1]+state.accPop[0])/state.nbrOfObs) + "(acc: "+state.accPop[1]+" nbrOfObs: "+state.nbrOfObs+")"	+ System.getProperty("line.separator") );
    	sb.append("Average time in system: " + state.accTimeInSystem/state.departures[1] + System.getProperty("line.separator"));
    	
    	
    	
    	System.out.print( sb.toString() );
    	
    }
}