figure(1);
delta = 1;
OF1_main;
plot(x,'--go');
hold on;
delta = 1.1;
OF1_main;
plot(x,':r*');
set(gca, 'XTickLabel',{'x11','x12','x13','x21','x22','x23','x31','x32','x33'})
legend('Delta 1','Delta 2','Location','northwest');
hold off;