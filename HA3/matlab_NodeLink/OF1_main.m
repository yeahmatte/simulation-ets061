% Varibales
% ---------------------------------
% Realization of demands - Xed
% x11 x21 x31 x41 x51 x61 - Realization of demand 1 on arc 1,2,3,4,5,6
% x12 x22 x32 x42 x52 x62 - Realization of demand 2 on arc 1,2,3,4,5,6
% x13 x23 x33 x43 x53 x63 - Realization of demand 3 on arc 1,2,3,4,5,6


% Constants
% ---------------------------------
% Demands
% h1 - Demand 1 From Vertex 1 to Vertex 3
% h2 - Demand 2 From Vertex 1 to Vertex 4
% h3 - Demand 3 From Vertex 1 to Vertex 5
h1 = 10;
h2 = 15;
h3 = 20;

% Link capacity
% c1 - Capacity arc 1
% c2 - Capacity arc 2
% c3 - Capacity arc 3
% c4 - Capacity arc 4
% c5 - Capacity arc 5
% c6 - Capacity arc 6
c1 = 10;
c2 = 20;
c3 = 30;
c4 = 40;
c5 = 40;
c6 = 40;

% Demand modifier
% delta - Common modifier for the demands
% delta =  // Set in delta-file

% x
% [x11 x21 x31 x41 x51 x61 x12 x22 x32 x42 x52 x62 x13 x23 x33 x43 x53 x63 z]

% A                               b
% x11 + x21 + x31               = h1 * delta
% x12 + x22 + x32               = h2 * delta
% x13 + x23 + x33               = h3 * delta
% x11-x11 x21-x21 ...           = 0
% -x41                          = -h1 * delta
% -x52                          = -h2 * delta
% -x63                          = -h3 * delta

% x11 + x12 + x13              <= c1 + z
% x21 + x22 + x23              <= c2 + z
% x31 + x32 + x33              <= c3 + z
% x41 + x42 + x43              <= c4 + z
% x51 + x52 + x53              <= c5 + z
% x61 + x62 + x63              <= c6 + z

% Linear equalities
%      x11         x12         x13         z
Aeq = [1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
       0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0;
       0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0;
       0 0 0 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
       0 0 0 0 0 0 0 0 0 0 -1 0 0 0 0 0 0 0 0;
       0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -1 0];
 
% Constraints for equalities
beq = [h1*delta; h2*delta; h3*delta;-h1*delta; -h2*delta; -h3*delta]; 

% Linear inequalities 
%        x11         x12         x13         z
Aineq = [1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 -1;
         0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 -1;
         0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 -1;
         0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 -1;
         0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 -1;
         0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 -1];

 
 % Constraints for inequalities
 bineq = [c1; c2; c3; c4; c5; c6]; 

 %Objective
 c = [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1];
 
 % Lower bound
 lowerBound = zeros(19,1);

options = optimoptions('linprog','Algorithm','simplex');
[x,fval,exitflag,output,lambda] = linprog(c,Aineq,bineq,Aeq,beq,lowerBound,[],[],options);
