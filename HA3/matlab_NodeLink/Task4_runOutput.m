%[x,fval,exitflag,output,lambda]
warning('off','all');
% OF1_delta1
OF1_delta1;
disp( sprintf( 'Objective function 1 - Delta 1' ) );
output
fval

% OF1_delta2
OF1_delta2;
disp( sprintf( 'Objective function 1 - Delta 2' ) );
output
fval

% OF1_delta3
OF1_delta3;
disp( sprintf( 'Objective function 1 - Delta 3' ) );
output
fval

% OF1_delta4
OF1_delta4;
disp( sprintf( 'Objective function 1 - Delta 4' ) );
output
fval

%%%%%%%%%%% OF 2 %%%%%%%%%%%%%%%%%%
% OF2_delta1
OF2_delta1;
disp( sprintf( 'Objective function 2 - Delta 1' ) );
output
fval

% OF2_delta2
OF2_delta2;
disp( sprintf( 'Objective function 2 - Delta 2' ) );
output
fval

% OF2_delta3
OF2_delta3;
disp( sprintf( 'Objective function 2 - Delta 3' ) );
output
fval

% OF2_delta4
OF2_delta4;
disp( sprintf( 'Objective function 2 - Delta 4' ) );
output
fval

%%%%%%%%%%% OF 3 %%%%%%%%%%%%%%%%%%
% OF3_delta1
OF3_delta1;
disp( sprintf( 'Objective function 3 - Delta 1' ) );
output
fval

% OF3_delta2
OF3_delta2;
disp( sprintf( 'Objective function 3 - Delta 2' ) );
output
fval

% OF3_delta3
OF3_delta3;
disp( sprintf( 'Objective function 3 - Delta 3' ) );
output
fval

% OF3_delta4
OF3_delta4;
disp( sprintf( 'Objective function 3 - Delta 4' ) );
output
fval


warning('on','all');