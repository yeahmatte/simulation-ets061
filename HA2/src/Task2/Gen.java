package Task2;

//Denna klass �rver Proc, det g�r att man kan anv�nda time och signalnamn utan punktnotation
//It inherits Proc so that we can use time and the signal names without dot notation 

class Gen extends Proc{
	private SignalList signalList;
	private GeneratorType type;
	
	//Generatorn har tv� parametrar:
	//There are two parameters:
	public Proc sendTo;    //Anger till vilken process de genererade kunderna ska skickas //Where to send customers

	public Gen(GeneratorType type) {
		this.type = type;
	}
	
	public void registerSignalList(SignalList signalList){
		this.signalList = signalList;
	}
	
	//H�r nedan anger man vad som ska g�ras n�r en signal kommer //What to do when a signal arrives
	public void TreatSignal(Signal x){
		switch (x.signalType){
			case READY:{
				if(time <= ENDTIME){
					signalList.SendSignal(ARRIVAL, sendTo, time);
					signalList.SendSignal(READY, this, time + type.getNextEventTime());
				}else{
					genEnded = true;
				}
			}break;
		}
	}
}