package Task2;

//Denna klass �rver Global s� att man kan anv�nda time och signalnamnen utan punktnotation
//It inherits Proc so that we can use time and the signal names without dot notation


public class MainSimulation extends Global{
	private Dispatcher dispatcher;
	private Gen generator;
	private QS queue;
	private SignalList signalList;
	
	public MainSimulation(){

	}
	
	
    public void run() {
    	//Signallistan startas och actSignal deklareras. actSignal �r den senast utplockade signalen i huvudloopen nedan.
    	// The signal list is started and actSignal is declaree. actSignal is the latest signal that has been fetched from the 
    	// signal list in the main loop below.
    	
    	time = 0;
    	ended = false;
    	genEnded = false;
    	
    	Signal actSignal;
    	signalList = new SignalList();

 
    	queue = new QS(signalList);
    	dispatcher = new Dispatcher();
    	generator = new Gen(new PoissonGenerator(0.25));
    	
    	generator.sendTo = dispatcher; //De genererade kunderna ska skickas till k�systemet QS  // The generated customers shall be sent to Q1
    	generator.registerSignalList(signalList);
    	dispatcher.registerSingalList(signalList);
    	dispatcher.registerQueue(queue);
    	
    	signalList.SendSignal(READY, generator, time);
    	
    	// Detta �r simuleringsloopen:
    	// This is the main loop

    	while (time < 100000 && !ended){
    		actSignal = signalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		
    		//System.out.println((actSignal==null)?"T:"+time:"F:"+time);
    		
    		actSignal.destination.TreatSignal(actSignal);
    		if(genEnded && queue.numberInQueue == 0)
    			break;
    	}
    }
    
    public double getClosingTime() {
    	return (queue.lastFinish < ENDTIME)?ENDTIME:queue.lastFinish;
    }
    
    public double getAverageServiceTime() {
    	double total = 0;
    	for(double d : queue.serviceTimes){
    		total += d;
    	}
    	
    	return total / queue.serviceTimes.size();
    }
    
    public int getNbrOfArrivals(){
    	return queue.serviceTimes.size();
    }
    
    public double getLastArrival(){
    	return queue.lastArrival;
    }
}