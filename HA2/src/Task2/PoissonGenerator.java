package Task2;

import java.util.Random;

public class PoissonGenerator implements GeneratorType {
	private double meanTime;
	private Random rnd;
	
	public PoissonGenerator(double meanTime){
		this.meanTime = meanTime;
		rnd = new Random();
	}
	
	public double getNextEventTime() {
		return (Math.log( 1 - rnd.nextDouble())  * (-1 * meanTime));
	}
}
