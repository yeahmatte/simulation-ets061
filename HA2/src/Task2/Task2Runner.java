package Task2;

import Task1.OutPuter;

public class Task2Runner {
	
	public static void main(String[] args) {

		OutPuter outer = new OutPuter("Task2_results.txt");
		double totTime = 0;
		double totClose = 0;
		double nbrOfObs = 0;
		
		for(int i=0;i<1000;i++) {
			nbrOfObs++;
			
			MainSimulation sim = new MainSimulation();
			sim.run();

			totTime += sim.getAverageServiceTime();
			totClose += sim.getClosingTime();
		}
		
		System.out.println("Home tot: "+ (totClose / nbrOfObs) + 9 );
		System.out.println("Avg service Time tot: "+ totTime / nbrOfObs);
		
		
		outer.print("\\begin{table}");
		outer.print("\\caption{Task 2 }");
		outer.print("\\label{table:task2_results}");
		outer.print("\\begin{center}");
		outer.print("\\begin{tabular}{L{3cm} L{2cm}}");
		outer.print("\\textbf{Measurement} & \\textbf{Value}  \\\\");
		outer.print("\\hline");
		
		
		outer.print("\\textbf{Average closing time} & "+ String.format("%2.0f : %2.0f", (totClose / nbrOfObs) + 9 , ((totClose/nbrOfObs) - Math.floor(totClose/nbrOfObs)) * 60) + " \\\\");
		outer.print("\\textbf{Average time before completed service} & "+String.format("%2.5f" ,totTime / nbrOfObs) + " Hours \\\\");
		outer.print("\\textbf{Number of observations} & "+ nbrOfObs +" \\\\");

		outer.print("\\end{tabular}");
		outer.print("\\end{center}");
		outer.print("\\end{table}");
		 
		outer.print("");
		
		//END
		System.out.println("Sim done");
		outer.close();
	}

	/*
	private static void doSim(Dispatcher d, Gen g, double interarrival, String task, String inter, String dis, OutPuter outer){
		MainSimulation sim = new MainSimulation(0.5);
		sim.run();
		outer.print("\\begin{table}[h]");
		outer.print("\\caption{"+ task + " " + inter + " "+ dis +"}");
		outer.print("\\label{table:"+ task.replaceAll(" ", "_") + inter.substring(14) + dis.substring(12).replaceAll(" ", "_") + "}");
		outer.print("\\begin{center}");
		outer.print("\\begin{tabular}{L{1cm} L{2cm} L{2cm} L{2cm}}");
		outer.print("\\textbf{Queue No.} & \\textbf{Avg population} & \\textbf{Accumelated population} & \\textbf{Nbr of measuerments} \\\\");
		outer.print("\\hline");

		outer.print(sim.getStatistics());

		outer.print("\\end{tabular}");
		outer.print("\\end{center}");
		outer.print("\\end{table}");
		 
		outer.print("");
	}

	
	/**
	 * 	StringBuilder sb = new StringBuilder();
    	
    	// ================ STATISTICS ===========================
    	double averageInSystem = 0;
    	
    	int i=1;
    	for(Proc p : queues){
    		QS qs = (QS) p;
    		sb.append("Queue "+(i++)+" & "+ String.format( "%2.5f" , ((double) qs.accumulated/qs.noMeasurements)) + " & "+qs.accumulated + " &	" + qs.noMeasurements + "\\\\"+ System.getProperty("line.separator") );
    		averageInSystem +=(double) qs.accumulated/qs.noMeasurements;
    	}
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("\\textbf{Total} & \\textbf{" +  String.format( "%2.5f" , averageInSystem) + "} & &  \\\\" + System.getProperty("line.separator"));
    	
    	return sb.toString();
	 */
}
