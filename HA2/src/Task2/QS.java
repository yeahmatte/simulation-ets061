package Task2;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


// This class defines a simple queuing system with one server. It inherits Proc so that we can use time and the
// signal names without dot notation
class QS extends Proc{
	public int numberInQueue = 0; //, accumulated, noMeasurements;
	Random rnd = new Random();
	private SignalList signalList;
	private Deque<Double> arrivalTimes;
	public List<Double> serviceTimes;
	public double lastArrival = 0;
	public double lastFinish = 0;
	
	public QS(SignalList signalList) {
		this.signalList = signalList;
		arrivalTimes = new LinkedList<Double>();
		serviceTimes = new LinkedList<Double>();
	}
	
	private double serviceTime(){		
		return ((0.166666) + ((0.1666666) * rnd.nextDouble()));
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				numberInQueue++;
				lastArrival = Double.valueOf(time);
				arrivalTimes.add(time);
				if (numberInQueue == 1){
					signalList.SendSignal(READY,this, time + serviceTime());
				}
			} break;

			case READY:{
				numberInQueue--;
				serviceTimes.add(time - arrivalTimes.poll());
				lastFinish = Double.valueOf(time);
				if (numberInQueue > 0){
					signalList.SendSignal(READY, this, time + serviceTime());
				}else if(time > ENDTIME){
					ended = true;
				}
			} break;

			case MEASURE:{
				//noMeasurements++;
				//accumulated = accumulated + numberInQueue;
				//signalList.SendSignal(MEASURE, this, time + 2*rnd.nextDouble());
			} break;
		}
	}
}