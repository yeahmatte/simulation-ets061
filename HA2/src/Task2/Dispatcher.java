package Task2;

public class Dispatcher extends Proc {
	private QS server;
	private SignalList signalList;

	public Dispatcher(){

	}

	public void TreatSignal(Signal x) {
		switch (x.signalType){
			case ARRIVAL:{
				signalList.SendSignal(ARRIVAL, server, time);
			} break;
	
			case READY:{
	
			} break;
	
			case MEASURE:{
	
			} break;
		}

	}

	public void registerSingalList(SignalList signalList){
		this.signalList = signalList;
	}
	
	public void registerQueue(QS q){
		server = q;
	}

}
