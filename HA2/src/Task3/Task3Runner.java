package Task3;

import Task1.OutPuter;

public class Task3Runner {
	
	public static void main(String[] args) {
		double totTime = 0;
		double nbrOfObs = 0;
		OutPuter outer = new OutPuter("Task3_results.txt");
		
		
		for(int i=0;i<1000;i++) {
			nbrOfObs++;
			
			MainSimulation sim = new MainSimulation();
			sim.run();

			totTime += sim.getBreakDownTime();	
		}
		
		System.out.println("Avg breakdown time: "+ totTime / nbrOfObs);
		
		
		outer.print("\\begin{table}");
		outer.print("\\caption{Task 3}");
		outer.print("\\label{table:task3_results}");
		outer.print("\\begin{center}");
		outer.print("\\begin{tabular}{L{3cm} L{2cm}}");
		outer.print("\\textbf{Measurement} & \\textbf{Value}  \\\\");
		outer.print("\\hline");
				
		outer.print("\\textbf{Average time before breakdown} & "+ String.format("%2.5f ", totTime / nbrOfObs) + " \\\\");;
		outer.print("\\textbf{Number of observations} & "+ nbrOfObs +" \\\\");

		outer.print("\\end{tabular}");
		outer.print("\\end{center}");
		outer.print("\\end{table}");
		 
		outer.print("");
		
		//END
		System.out.println("Sim done");
		outer.close();
	}
}
