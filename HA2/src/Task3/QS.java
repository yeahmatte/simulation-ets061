package Task3;
import java.util.*;
import java.io.*;

// This class defines a simple queuing system with one server. It inherits Proc so that we can use time and the
// signal names without dot notation
class QS extends Proc{
	public int numberInQueue = 0;
	public List<Proc> sendTo;
	private Random rnd = new Random();
	private SignalList signalList;
	private int queueNo;
	
	public QS(int queueNo, SignalList signalList) {
		this.sendTo = new LinkedList<Proc>();
		this.queueNo = queueNo;
		this.signalList = signalList;
	}
	
	private double serviceTime(){
		
		return (rnd.nextDouble() * 4.0) + 1.0;
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				numberInQueue++;
				if (numberInQueue == 1){
					signalList.SendSignal(READY,this, time + serviceTime());
				}
			} break;

			case READY:{
				numberInQueue--;
				broken[queueNo] = true;
				for(Proc p : sendTo){
					signalList.SendSignal(READY, p, time);
				}
			} break;

			case MEASURE:{
				
			} break;
		}
	}
	
	public void registerRecipent(Proc p) {
		sendTo.add(p);
	}
}