package Task3;

import java.util.ArrayList;
import java.util.List;

//Denna klass �rver Global s� att man kan anv�nda time och signalnamnen utan punktnotation
//It inherits Proc so that we can use time and the signal names without dot notation


public class MainSimulation extends Global{
	private List<Proc> queues;
	private SignalList signalList;
	private double breakdownTime;
	
	
	public MainSimulation(){
	
	}
	
	
    public void run() {
    	
    	time = 0;
    	broken = new boolean[5];
    	
    	for(int i=0;i<5;i++)
    		broken[i] = false;
    	
    	breakdownTime = 0;
    	
    	Signal actSignal;
    	signalList = new SignalList();

    	//H�r nedan skapas de processinstanser som beh�vs och parametrar i dem ges v�rden.
    	// Here process instances are created (two queues and one generator) and their parameters are given values. 
    	
    	queues = new ArrayList<Proc>();
    	for(int i=0;i<5;i++)
    		queues.add(new QS(i,signalList));
    	
    	((QS) queues.get(0)).registerRecipent(queues.get(1));
    	((QS) queues.get(0)).registerRecipent(queues.get(4));
    	((QS) queues.get(2)).registerRecipent(queues.get(3));
    	
    	    	
/**
 * Assume that we have a system that consists of five components; each one of them has a uniformly
distributed life length in the interval from 1 to 5. We also assume that if component 1 breaks down,
also component 2 and 5 breaks down and if component 3 breaks down also component 4 breaks
down. There are no other dependencies between the life lengths of the components. The system
works as long as at least one component works.
 */
    	
    	
    	//H�r nedan skickas de f�rsta signalerna f�r att simuleringen ska komma ig�ng.
    	//To start the simulation the first signals are put in the signal list

    	for(Proc p : queues)
    		signalList.SendSignal(ARRIVAL, p, time);
    	

    	// Detta �r simuleringsloopen:
    	// This is the main loop

    	while (time < 100000){
    		actSignal = signalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		actSignal.destination.TreatSignal(actSignal);
    		
    		if(broken[0] && broken[1] && broken[2] && broken[3] && broken[4]){
    			breakdownTime = Double.valueOf(time);
    			break;
    		}
    	}
    }
    
    public double getBreakDownTime() {
    	return breakdownTime;
    }
}