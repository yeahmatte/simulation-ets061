package Task1;

public class Task1Runner {
	
	public static void main(String[] args) {

		OutPuter outer = new OutPuter("Task1_results.txt");
			
		
		// ======================= TASK 1 B) ===========================
		
		// TASK 1 b) Random
		doSim( new Dispatcher(new RandomDispatcher()), new Gen(new UniformedGenerator(0.12)), 
				0.5 , "Task 1 B","Interarrival: 0.12s","Dispatcher: Random", outer);
		
		// TASK 1 b) Round Robin
		doSim(new Dispatcher(new RoundRobinDispatcher()), new Gen(new UniformedGenerator(0.12)),
				0.5 , "Task 1 B","Interarrival: 0.12s","Dispatcher: Round Robin", outer);
		
		// TASK 1 b) Min Queue Length
		doSim(new Dispatcher(new MinQueueLengthDispatcher()), new Gen(new UniformedGenerator(0.12)),
	 			0.5, "Task 1 B","Interarrival: 0.12s","Dispatcher: Min Queue Length", outer);
		
		// ======================= TASK 1 C) 0.11 ===========================
		
		// TASK 1 c) 0.11 Random
		doSim(new Dispatcher(new RandomDispatcher()), new Gen(new UniformedGenerator(0.11)), 
				0.5, "Task 1 C","Interarrival: 0.11s","Dispatcher: Random", outer);

		// TASK 1 c) 0.11 Round Robin
		doSim(new Dispatcher(new RoundRobinDispatcher()),
		 new Gen(new UniformedGenerator(0.11)),0.5,
		 "Task 1 C","Interarrival: 0.11s","Dispatcher: Round Robin", outer);
		
		// TASK 1 c) 0.11 Min Queue Length
		doSim(new Dispatcher(new MinQueueLengthDispatcher()),
		 new Gen(new UniformedGenerator(0.11)),0.5,
		 "Task 1 C","Interarrival: 0.11s","Dispatcher: Min Queue Length", outer);

		
		// ======================= TASK 1 C) 0.15 ===========================
		
		// TASK 1 c) 0.15 Random
		doSim(new Dispatcher(new RandomDispatcher()),
		 new Gen(new UniformedGenerator(0.15)),0.5,
		 "Task 1 C","Interarrival: 0.15s","Dispatcher: Random", outer);

		
		
		// TASK 1 c) 0.15 Round Robin
		doSim(new Dispatcher(new RoundRobinDispatcher()),
		 new Gen(new UniformedGenerator(0.15)), 0.5,
				 "Task 1 C","Interarrival: 0.15s","Dispatcher: Round Robin ", outer);

		
		// TASK 1 c) 0.15 Min Queue Length
		doSim(new Dispatcher(new MinQueueLengthDispatcher()),
		 new Gen(new UniformedGenerator(0.15)), 0.5, 
		 "Task 1 C","Interarrival: 0.15s","Dispatcher: Min Queue Length", outer);

		
		// ======================= TASK 1 C) 2.00 ===========================
		
		// TASK 1 c) 2.00 Random
		doSim(new Dispatcher(new RandomDispatcher()), new Gen(new UniformedGenerator(0.11)),0.5,
		 "Task 1 C" ,"Interarrival: 2.00s","Dispatcher: Random ", outer);
		
		
		// TASK 1 c) 2.00 Round Robin
		doSim(new Dispatcher(new RoundRobinDispatcher()), 
		 new Gen(new UniformedGenerator(2)), 0.5, "Task 1 c)" ,"Interarrival: 2.00s","Dispatcher: Round Robin", outer);
		
		// TASK 1 c) 2.00 Min Queue Length
		doSim(new Dispatcher(new MinQueueLengthDispatcher()),
		 new Gen(new UniformedGenerator(2)), 0.5,
		 "Task 1 C" ,"Interarrival: 2.00s","Dispatcher: Min Queue Length", outer);
		
		
		
		//END
		System.out.println("Sim done");
		outer.close();
	}

	
	private static void doSim(Dispatcher d, Gen g, double interarrival, String task, String inter, String dis, OutPuter outer){
		MainSimulation sim = new MainSimulation(d,g,0.5);
		sim.run();
		outer.print("\\begin{table}");
		outer.print("\\caption{"+ task + " " + inter + " "+ dis +"}");
		outer.print("\\label{table:"+ task.replaceAll(" ", "_") + inter.substring(14) + dis.substring(12).replaceAll(" ", "_") + "}");
		outer.print("\\begin{center}");
		outer.print("\\begin{tabular}{L{1cm} L{2cm} L{2cm} L{2cm}}");
		outer.print("\\textbf{Queue No.} & \\textbf{Avg population} & \\textbf{Accumelated population} & \\textbf{Nbr of measuerments} \\\\");
		outer.print("\\hline");

		outer.print(sim.getStatistics());

		outer.print("\\end{tabular}");
		outer.print("\\end{center}");
		outer.print("\\end{table}");
		 
		outer.print("");
	}

}
