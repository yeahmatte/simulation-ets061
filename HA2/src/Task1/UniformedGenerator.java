package Task1;

import java.util.Random;

public class UniformedGenerator implements GeneratorType {
	private double meanTime;
	private Random rnd;
	
	public UniformedGenerator(double meanTime){
		this.meanTime = meanTime;
		rnd = new Random();
	}
	
	public double getNextEventTime() {
		
		return rnd.nextDouble() * 2 * meanTime;
		//return Double.valueOf(meanTime);
	}

}
