package Task1;
import java.util.*;
import java.io.*;

// This class defines a simple queuing system with one server. It inherits Proc so that we can use time and the
// signal names without dot notation
class QS extends Proc{
	public int numberInQueue = 0, accumulated, noMeasurements;
	public Proc sendTo;
	Random rnd = new Random();
	private double meanServiceTime;
	private SignalList signalList;

	public QS(double serviceTime, Proc sendTo, SignalList signalList) {
		this.sendTo = sendTo;
		this.meanServiceTime = serviceTime;
		this.signalList = signalList;
	}
	
	private double serviceTime(){
		
		return (Math.log( 1 - rnd.nextDouble())  * (-1 * meanServiceTime));
	}
	
	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				numberInQueue++;
				if (numberInQueue == 1){
					signalList.SendSignal(READY,this, time + serviceTime());
				}
			} break;

			case READY:{
				numberInQueue--;
				if (sendTo != null){
					signalList.SendSignal(ARRIVAL, sendTo, time);
				}
				if (numberInQueue > 0){
					signalList.SendSignal(READY, this, time + serviceTime());
				}
			} break;

			case MEASURE:{
				noMeasurements++;
				accumulated = accumulated + numberInQueue;
				signalList.SendSignal(MEASURE, this, time + 2*rnd.nextDouble());
			} break;
		}
	}
}