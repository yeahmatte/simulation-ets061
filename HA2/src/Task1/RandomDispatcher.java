package Task1;

import java.util.List;
import java.util.Random;

public class RandomDispatcher implements DispatchType {
	private int nbrOfQueues;
	private Random rnd;
	
	
	public RandomDispatcher(){
		rnd = new Random();
	}

	public int nextQueue() {
		return rnd.nextInt(nbrOfQueues);
	}

	public void registerQueues(List<Proc> l) {
		nbrOfQueues = l.size();
	}

}
