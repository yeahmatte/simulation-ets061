package Task1;

import java.util.List;

public interface DispatchType {
	public int nextQueue();
	public void registerQueues(List<Proc> l);
}
