package Task1;

import java.util.List;

public class Dispatcher extends Proc {
	private List<Proc> servers;
	private DispatchType type;
	private SignalList signalList;

	public Dispatcher(DispatchType type){
		this.type = type;
	}

	public void TreatSignal(Signal x) {
		switch (x.signalType){
			case ARRIVAL:{
				signalList.SendSignal(ARRIVAL, servers.get(type.nextQueue()), time);
			} break;
	
			case READY:{
	
			} break;
	
			case MEASURE:{
	
			} break;
		}

	}

	public void registerSingalList(SignalList signalList){
		this.signalList = signalList;
	}
	
	public void registerQueues(List<Proc> l){
		servers = l;
		type.registerQueues(l);
	}

}
