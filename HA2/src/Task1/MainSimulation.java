package Task1;

import java.util.LinkedList;
import java.util.List;

//Denna klass �rver Global s� att man kan anv�nda time och signalnamnen utan punktnotation
//It inherits Proc so that we can use time and the signal names without dot notation


public class MainSimulation extends Global{
	private Dispatcher dispatcher;
	private Gen generator;
	private double meanServiceTime;
	private List<Proc> queues;
	private SignalList signalList;
	
	public MainSimulation(Dispatcher d, Gen g, double mst){
		this.dispatcher = d;
		this.generator = g;
		this.meanServiceTime = mst;
	}
	
	
    public void run() {
    	//Signallistan startas och actSignal deklareras. actSignal �r den senast utplockade signalen i huvudloopen nedan.
    	// The signal list is started and actSignal is declaree. actSignal is the latest signal that has been fetched from the 
    	// signal list in the main loop below.
    	
    	time = 0;
    	
    	Signal actSignal;
    	signalList = new SignalList();

    	//H�r nedan skapas de processinstanser som beh�vs och parametrar i dem ges v�rden.
    	// Here process instances are created (two queues and one generator) and their parameters are given values. 
    	queues = new LinkedList<Proc>(); 
    	for(int i=1;i<=5;i++)
    	  queues.add(new QS(meanServiceTime,null,signalList));


    	
    	generator.sendTo = dispatcher; //De genererade kunderna ska skickas till k�systemet QS  // The generated customers shall be sent to Q1
    	generator.registerSignalList(signalList);
    	dispatcher.registerQueues(queues);
    	dispatcher.registerSingalList(signalList);
    	
    	//H�r nedan skickas de f�rsta signalerna f�r att simuleringen ska komma ig�ng.
    	//To start the simulation the first signals are put in the signal list

    	signalList.SendSignal(READY, generator, time);
    	for(Proc p : queues)
    		signalList.SendSignal(MEASURE, p, time);


    	// Detta �r simuleringsloopen:
    	// This is the main loop

    	while (time < 100000){
    		actSignal = signalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		actSignal.destination.TreatSignal(actSignal);
    	}
    }
    
    public String getStatistics() {
    	StringBuilder sb = new StringBuilder();
    	
    	// ================ STATISTICS ===========================
    	double averageInSystem = 0;
    	
    	int i=1;
    	for(Proc p : queues){
    		QS qs = (QS) p;
    		sb.append("Queue "+(i++)+" & "+ String.format( "%2.5f" , ((double) qs.accumulated/qs.noMeasurements)) + " & "+qs.accumulated + " &	" + qs.noMeasurements + "\\\\"+ System.getProperty("line.separator") );
    		averageInSystem +=(double) qs.accumulated/qs.noMeasurements;
    	}
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("\\textbf{Total} & \\textbf{" +  String.format( "%2.5f" , averageInSystem) + "} & &  \\\\" + System.getProperty("line.separator"));
    	
    	return sb.toString();
    }
}