package Task1;

import java.util.List;

public class RoundRobinDispatcher implements DispatchType {
	int lastQueue;
	int nbrOfQueues;
	
	public RoundRobinDispatcher() {
		lastQueue = nbrOfQueues-1; 
	}
	
	public int nextQueue() {
		if( lastQueue == nbrOfQueues-1 ){
			lastQueue = 0;
		}else{
			lastQueue++;
		}
		
		return lastQueue;
	}

	public void registerQueues(List<Proc> l) {
		nbrOfQueues = l.size();
	}
}
