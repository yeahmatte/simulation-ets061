\begin{table}
\caption{Task 3}
\label{table:task3_results}
\begin{center}
\begin{tabular}{L{3cm} L{2cm}}
\textbf{Measurement} & \textbf{Value}  \\
\hline
\textbf{Average time before breakdown} & 3,67945  \\
\textbf{Number of observations} & 1000.0 \\
\end{tabular}
\end{center}
\end{table}

