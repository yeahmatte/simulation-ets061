clc; clear all;

% Note that in this simulation, links are indexed in breath-wise fashion,
%  starting from the upper breath and incrementing from left to right in the
%  breath

delta = 1;

% constants
h = [10 * delta, 15 * delta, 20 * delta]; % demand vector
z = [30, 20, 10, 0, 0, 0]; % link cost vector. 
c = [40, 40, 40, 10, 20, 30]; % link capacity vector

D = length(h); % number of unique demands
E = length(c); % number of links

% define flow variable
variables = {'U_11','U_12','U_13', ...
             'U_21','U_22','U_23', ...
             'U_31','U_32','U_33'};
N = length(variables); 
% create variables for indexing 
for v = 1:N 
   eval([variables{v},' = ', num2str(v),';']); 
end

% set lower bounds
lb = zeros(size(variables));
% lb(Z) = -Inf;
% set upper bounds
ub = ones(size(variables));
% ub(Z) = Inf;

% define inequality constraints
A = zeros(E,N);
A(1,[U_11, U_12, U_13]) = [h(1),h(1),h(1)]; b(1) = c(1);
A(2,[U_21, U_22, U_23]) = [h(2),h(2),h(2)]; b(2) = c(2);
A(3,[U_31, U_32, U_33]) = [h(3),h(3),h(3)]; b(3) = c(3);
A(4,[U_11, U_21, U_31]) = [h(1),h(2),h(3)]; b(4) = c(4);
A(5,[U_12, U_22, U_32]) = [h(1),h(2),h(3)]; b(5) = c(5);
A(6,[U_13, U_23, U_33]) = [h(1),h(2),h(3)]; b(6) = c(6);


% define equality constraints
Aeq = zeros(D,N); beq = zeros(D,1);
Aeq(1,[U_11, U_12, U_13]) = [1,1,1]; beq(1) = 1;
Aeq(2,[U_21, U_22, U_23]) = [1,1,1]; beq(2) = 1;
Aeq(3,[U_31, U_32, U_33]) = [1,1,1]; beq(3) = 1;

% define integer constrains
intcon = [U_11 U_12 U_13 U_21 U_22 U_23 U_31 U_32 U_33];

% define the (minimization) objective function. The vector on the right is
% claculated manually by summing over the costs of all links along the path
f = zeros(size(variables));
f([U_11 U_12 U_13 U_21 U_22 U_23 U_31 U_32 U_33]) = [30 20 10 30 20 10 30 20 10];

% run the optimization function. The complete format is x = linprog(f,A,b,Aeq,beq,lb,ub,x0). 
[x fval] = intlinprog(f,intcon,A,b,Aeq,beq,lb,ub);
for d = 1:N
  fprintf('%12.2f \t%s\n',x(d),variables{d}) 
end

S = 'The value of objective function at the optimal point: ';
fprintf('%s %12.2f \n', S, fval);
