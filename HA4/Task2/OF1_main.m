% Varibales
% ---------------------------------
% Realization of demands
% x11 x12 x13 - Realization of demand 1 on path 1,2,3
% x21 x22 x23 - Realization of demand 2 on path 1,2,3
% x31 x32 x33 - Realization of demand 3 on path 1,2,3
%
% Constants
% ---------------------------------
% Demands
% h1 - Demand 1 From Vertex 1 to Vertex 3
% h2 - Demand 2 From Vertex 1 to Vertex 4
% h3 - Demand 3 From Vertex 1 to Vertex 5
h1 = 10;
h2 = 15;
h3 = 20;

% Link capacity
% c1 - Capacity arc 1
% c2 - Capacity arc 2
% c3 - Capacity arc 3
% c4 - Capacity arc 4
% c5 - Capacity arc 5
% c6 - Capacity arc 6
c1 = 10;
c2 = 20;
c3 = 30;
c4 = 40;
c5 = 40;
c6 = 40;

% Demand modifier
% delta - Common modifier for the demands
% delta =  // Set in delta-file

% Model
% ---------------------------------
% z = cx
% Ax = b
% x >= 0
%

% x
% [x11 x12 x13 x21 x22 x23 x31 x32 x33]

% A                               b
% x11 + x12 + x13               = h1 * delta
% x21 + x22 + x23               = h2 * delta
% x31 + x32 + x33               = h3 * delta
% x11 + x21 + x31              <= c1
% x12 + x22 + x32 + a2         <= c2
% x13 + x23 + x33 + a3         <= c3
% x11 + x12 + x13 + a4         <= c4
% x21 + x22 + x23 + a5         <= c5
% x31 + x32 + x33 + a6         <= c6

% Linear equalities 
Aeq = [1 1 1 0 0 0 0 0 0;
     0 0 0 1 1 1 0 0 0;
     0 0 0 0 0 0 1 1 1];
% Linear inequalities 
Aineq =[1 0 0 1 0 0 1 0 0;
     0 1 0 0 1 0 0 1 0;
     0 0 1 0 0 1 0 0 1;
     1 1 1 0 0 0 0 0 0;
     0 0 0 1 1 1 0 0 0;
     0 0 0 0 0 0 1 1 1];
 
 % Constraints for equalities
 beq = [h1*delta; h2*delta; h3*delta];
 
 % Constraints for inequalities
 bineq = [c1; c2; c3; c4; c5; c6]; 

 %Objective
 c = [1; 1; 1; 1; 1; 1; 1; 1; 1];
 
 % Lower bound
 lowerBound = zeros(9,1);

options = optimoptions('linprog','Algorithm','simplex');
[x,fval,exitflag,output,lambda] = linprog(c,Aineq,bineq,Aeq,beq,lowerBound,[],[],options);
