% Varibales
% ---------------------------------
% Realization of demands
% x11 x12 x13 - Realization of demand 1 on path 1,2,3
% x21 x22 x23 - Realization of demand 2 on path 1,2,3
% x31 x32 x33 - Realization of demand 3 on path 1,2,3


% Model
% ---------------------------------
% z = cx
% Ax = b
% x >= 0
%


% Linear equalities 
Aeq = [1 1 1 1 0
       1 0 0 0 0];

% Linear inequalities 
Aineq =[12 0 0 0 -1;
        0 12 0 0 -1;
        0 0 12 0 -1;
        0 0 0 12 -1];
 
 % Constraints for equalities
 beq = [1; 1];
 
 % Constraints for inequalities
 bineq = [1; 2; 3; 4]; 

 %Objective
 c = [0; 0; 0; 0; 1];
 
 % Lower bound
 lowerBound = zeros(5,1);

options = optimoptions('linprog','Algorithm','simplex');
[x,fval,exitflag,output,lambda] = linprog(c,Aineq,bineq,Aeq,beq,lowerBound,[],[],options);
