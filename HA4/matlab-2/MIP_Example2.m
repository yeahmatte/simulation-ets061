clc; clear all;

% Note that in this simulation, links are indexed in breath-wise fashion,
%  starting from the upper breath and incrementing from left to right in the
%  breath

% constants
h = [10, 10, 10]; % demand vector
z = [1, 1, 1, 1, 1, 1, 1, 1, 1]; % link cost vector. 
c = [10, 10, 10, 5, 15, 10, 5, 15, 10]; % link capacity vector

D = length(h); % number of unique demands
E = length(c); % number of links

% define flow variable
variables = {'X_11','X_12','X_13', ...
             'X_21','X_22','X_23', ...
             'X_31','X_32','X_33'};
N = length(variables); 
% create variables for indexing 
for v = 1:N 
   eval([variables{v},' = ', num2str(v),';']); 
end

% set lower bounds
lb = zeros(size(variables));

% define inequality constraints
A = zeros(E,N);
A(1,[X_11, X_12, X_13]) = [1,1,1]; b(1) = c(1);
A(2,[X_21, X_22, X_23]) = [1,1,1]; b(2) = c(2);
A(3,[X_31, X_32, X_33]) = [1,1,1]; b(3) = c(3);
A(4,[X_11, X_21, X_31]) = [1,1,1]; b(4) = c(4);
A(5,[X_12, X_22, X_32]) = [1,1,1]; b(5) = c(5);
A(6,[X_13, X_23, X_33]) = [1,1,1]; b(6) = c(6);
A(7,[X_11, X_21, X_31]) = [1,1,1]; b(7) = c(7);
A(8,[X_12, X_22, X_32]) = [1,1,1]; b(8) = c(8);
A(9,[X_13, X_23, X_33]) = [1,1,1]; b(9) = c(9);

% define equality constraints
Aeq = zeros(D,N); beq = zeros(3,1);
Aeq(1,[X_11, X_12, X_13]) = [1,1,1]; beq(1) = h(1);
Aeq(2,[X_21, X_22, X_23]) = [1,1,1]; beq(2) = h(2);
Aeq(3,[X_31, X_32, X_33]) = [1,1,1]; beq(3) = h(3);

% define integer constrains
intcon = [X_11 X_12 X_13 X_21 X_22 X_23 X_31 X_32 X_33];

% define the (minimization) objective function. The vector on the right is
% claculated manually by summing over the costs of all links along the path
f = zeros(size(variables));
f([X_11 X_12 X_13 X_21 X_22 X_23 X_31 X_32 X_33]) = [3 3 3 3 3 3 3 3 3];

% run the optimization function. The complete format is x = linprog(f,A,b,Aeq,beq,lb,ub,x0). 
[x fval] = intlinprog(f,intcon,A,b,Aeq,beq,lb);
for d = 1:N
  fprintf('%12.2f \t%s\n',x(d),variables{d}) 
end

S = 'The value of objective function at the optimal point: ';
fprintf('%s %12.2f \n', S, fval);