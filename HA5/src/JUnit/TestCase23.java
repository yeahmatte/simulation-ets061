package JUnit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Task1.Bag;
import Task1.KnapSack;
import Task1.KnapSackItem;
import static org.junit.Assert.assertEquals;


public class TestCase23 {

	private static KnapSack knapsack;
	private static final String DATFILEPATH = "C:\\Users\\yeahmatte\\Documents\\Git\\simulation-ets061\\HA5\\data\\test3-in.txt";


	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		Scanner data = new Scanner(new File(DATFILEPATH));
		knapsack = new KnapSack(data);

		System.out.println(" --- Test Case 3 ---");
	}

	/** 
	 * Initializes common objects. The JUnit framework automatically invokes 
	 * this method before each test is run.
	 * @throws FileNotFoundException 
	 */
	@Before
	public void setUp() 
	{


	}

	/**
	 * Cleanup method. The JUnit framework automatically invokes
	 * this method after each test is run.
	 */
	@After
	public void tearDown()
	{
		//nada
	}

	@AfterClass
	public static void afterClass(){
		//Nada;
	}


	@Test
	public void outputTest() throws FileNotFoundException {

		// Nada;
	}


	@Test
	public void KnapSackProbabiltyTest1() {
		
		Bag propBag = knapsack.findKnapSack(100000, 0.9999, 10);
		
		
		System.out.println("Probability Bag 1");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Total Weight: "+ propBag.getWeight());
		System.out.println("Total Value: "+ propBag.getValue());
	}
	
	@Test
	public void KnapSackProbabiltyTest2() {
		
		Bag propBag = knapsack.findKnapSack(100000, 0.99, 30);
		
		
		System.out.println("Probability Bag 2");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Total Weight: "+ propBag.getWeight());
		System.out.println("Total Value: "+ propBag.getValue());
	}
	
	@Test
	public void KnapSackProbabiltyTest3() {
		
		Bag propBag = knapsack.findKnapSack(100000, 0.9, 50);
		
		
		System.out.println("Probability Bag 3");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Total Weight: "+ propBag.getWeight());
		System.out.println("Total Value: "+ propBag.getValue());
	}
	
	@Test
	public void KnapSackBruteForeceTest() {
	
		Bag bfBag = knapsack.startBruteforce();
		
		System.out.println("BFF Bag");
		//knapsack.printKnapSackList(bfBag.getItems());
		System.out.println("Total Weight: "+ bfBag.getWeight());
		System.out.println("Total Value: "+ bfBag.getValue());
	}

}