package JUnit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Task1.Bag;
import Task1.KnapSack;
import Task1.KnapSackItem;
import static org.junit.Assert.assertEquals;


public class TestCase22 {

	private static KnapSack knapsack;
	//private static final String DATFILEPATH = "C:\\Users\\yeahmatte\\Documents\\Git\\simulation-ets061\\HA5\\data\\test2-in.txt";
	private static final String DATFILEPATH = "C:\\Users\\yeahmatte\\Documents\\Git\\Simulation\\HA5\\data\\test2-in.txt";

	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		Scanner data = new Scanner(new File(DATFILEPATH));
		knapsack = new KnapSack(data);

		System.out.println(" --- Test Case 2 ---");
	}

	/** 
	 * Initializes common objects. The JUnit framework automatically invokes 
	 * this method before each test is run.
	 * @throws FileNotFoundException 
	 */
	@Before
	public void setUp() 
	{


	}

	/**
	 * Cleanup method. The JUnit framework automatically invokes
	 * this method after each test is run.
	 */
	@After
	public void tearDown()
	{
		//nada
	}

	@AfterClass
	public static void afterClass(){
		//Nada;
	}


	@Test
	public void outputTest() throws FileNotFoundException {

		// Nada;
	}

/*
	@Test
	public void KnapSackSingleProbabiltyTest() {

		Bag propBag = knapsack.findKnapSack(100000, 0.9999, 10);


		System.out.println("Single Probability Bag");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Total Weight: "+ propBag.getWeight());
		System.out.println("Total Value: "+ propBag.getValue());
	}
	
	@Test
	public void KnapSackStatsProbabiltyTestConfig1() {

		int weight = 0;
		int value = 0;
		int nbrOfRuns = 100;
		for(int i = 0; i < nbrOfRuns; i++) {
			Bag propBag = knapsack.findKnapSack(100000, 0.9999, 10);
			weight += propBag.getWeight();
			value += propBag.getValue();
		}
		System.out.println("Avg of 100 Probability Bag - config 1");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Avg Weight: "+ ((double) weight)/nbrOfRuns );
		System.out.println("Avg Value: "+ ((double) value)/nbrOfRuns);
	}

	@Test
	public void KnapSackStatsProbabiltyTestConfig2() {

		int weight = 0;
		int value = 0;
		int nbrOfRuns = 100;
		for(int i = 0; i < nbrOfRuns; i++) {
			Bag propBag = knapsack.findKnapSack(100000, 0.9999, 5);
			weight += propBag.getWeight();
			value += propBag.getValue();
		}
		System.out.println("Avg of 100 Probability Bag - config 2");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Avg Weight: "+ ((double) weight)/nbrOfRuns );
		System.out.println("Avg Value: "+ ((double) value)/nbrOfRuns);
	}

	@Test
	public void KnapSackMaxProbabiltyTest() {

		int weight = 0;
		int value = 0;
		int nbrOfRuns = 10;
		for(int i = 0; i < nbrOfRuns; i++) {
			Bag propBag = knapsack.findKnapSack(100000, 0.9999, 10);
			if(propBag.getValue() > value) {
				weight = propBag.getWeight();
				value = propBag.getValue();
			}
		}

		System.out.println("Best Probability Bag");
		//knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Avg Weight: "+ weight );
		System.out.println("Avg Value: "+ value);
	}
	 */

	@Test
	public void KnapSackMaxStatProbabiltyTest() {

		System.out.println("Best Probability STATISICS Bag");
		for(int n=1; n<=20; n++){
			int correctRuns = 0;
			int nbrOfTrails = n;
			int nbrOfRuns = 100;

			for(int j = 0; j < nbrOfRuns; j++){
				int value = 0;
				int weight = 0;

				for(int i = 0; i < nbrOfTrails; i++) {
					Bag propBag = knapsack.findKnapSack(10000, 0.999, 10);
					if(propBag.getValue() > value) {
						weight = propBag.getWeight();
						value = propBag.getValue();
					}
				}

				if(value == 65 && weight == 42){
					correctRuns++;
				}
			}

			//knapsack.printKnapSackList(propBag.getItems());
			System.out.println("" + nbrOfTrails + "	"+ (((double) correctRuns)/nbrOfRuns ) + " b");
		}
	}

	@Test
	public void KnapSackBruteForeceTest() {

		Bag bfBag = knapsack.startBruteforce();

		System.out.println("BFF Bag");
		//knapsack.printKnapSackList(bfBag.getItems());
		System.out.println("Total Weight: "+ bfBag.getWeight());
		System.out.println("Total Value: "+ bfBag.getValue());
	}

}