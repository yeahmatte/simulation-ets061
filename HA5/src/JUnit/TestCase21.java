package JUnit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Task1.Bag;
import Task1.KnapSack;
import Task1.KnapSackItem;

import static org.junit.Assert.assertEquals;


public class TestCase21 {

	private static KnapSack knapsack;
	private static final String DATFILEPATH = System.getProperty("user.dir") + "\\data\\test1-in.txt";


	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		Scanner data = new Scanner(new File(DATFILEPATH));
		knapsack = new KnapSack(data);
		
		System.out.println(" --- Test Case 1 ---");
	}

	/** 
	 * Initializes common objects. The JUnit framework automatically invokes 
	 * this method before each test is run.
	 * @throws FileNotFoundException 
	 */
	@Before
	public void setUp() 
	{


	}

	/**
	 * Cleanup method. The JUnit framework automatically invokes
	 * this method after each test is run.
	 */
	@After
	public void tearDown()
	{
		//nada
	}

	@AfterClass
	public static void afterClass(){
		//Nada;
	}


	@Test
	public void outputTest() throws FileNotFoundException {

		// Nada;
	}

	@Test
	public void KnapSackProbabiltyTest1() {		
		Bag propBag = knapsack.findKnapSack(100000, 0.9999, 10);
		
		System.out.println("--- Probability Bag---");
		knapsack.printKnapSackList(propBag.getItems());
		System.out.println("Total Weight: "+ propBag.getWeight());
		System.out.println("Total Value: "+ propBag.getValue());
	}
	
	@Test
	public void KnapSackProbabiltyGreedy() {
		Bag greedyBag = knapsack.findKnapSackGreedy(100000, 0.9999, 10);
			
		System.out.println("--- Greedy ---");
		knapsack.printKnapSackList(greedyBag.getItems());
		System.out.println("Total Weight: "+ greedyBag.getWeight());
		System.out.println("Total Value: "+ greedyBag.getValue());
	}	
	
	@Test
	public void KnapSackBruteForeceTest() {
		Bag bfBag = knapsack.startBruteforce();
		
		System.out.println("--- Brute Force Bag ----");
		knapsack.printKnapSackList(bfBag.getItems());
		System.out.println("Total Weight: "+ bfBag.getWeight());
		System.out.println("Total Value: "+ bfBag.getValue());
	}

}