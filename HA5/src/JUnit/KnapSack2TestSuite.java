package JUnit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TestCase21.class,TestCase22.class,TestCase23.class})
public class KnapSack2TestSuite {

}
