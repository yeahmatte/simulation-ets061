package JUnit;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Task1.Bag;
import Task1.KnapSack;
import Task1.KnapSackItem;
//import static org.junit.Assert.assertEquals;


public class TestCase3 {

	private static KnapSack knapsack;
	private static final String DATFILEPATH = System.getProperty("user.dir") + "\\data\\test-m6.txt";


	@BeforeClass
	public static void beforeClass() throws FileNotFoundException {
		Scanner data = new Scanner(new File(DATFILEPATH));
		knapsack = new KnapSack(data);

	}

	/** 
	 * Initializes common objects. The JUnit framework automatically invokes 
	 * this method before each test is run.
	 * @throws FileNotFoundException 
	 */
	@Before
	public void setUp() 
	{


	}

	/**
	 * Cleanup method. The JUnit framework automatically invokes
	 * this method after each test is run.
	 */
	@After
	public void tearDown()
	{
		//spanningUSA = null;
	}

	@AfterClass
	public static void afterClass(){
		knapsack = null;
	}

	@Test
	public void Task1_M6() {
		int value = 0;
		int weight = 0;
		int nbrOfTrails = 15;
		
		for(int i = 0; i < nbrOfTrails; i++) {
			Bag propBag = knapsack.findKnapSack(10000, 0.999, 10);
			if(propBag.getValue() > value) {
				weight = propBag.getWeight();
				value = propBag.getValue();
			}
		}
		
		
		System.out.println("--- KnapSack M=6 ----");
		System.out.println("Trails: "+nbrOfTrails);
		System.out.println("Value: "+value);
		System.out.println("Weight: "+weight);		
		
		//Brute force
		//Bag bf = knapsack.startBruteforce();
		//System.out.println("BF value:" + bf.getValue());
	}

}