package Task1;

import java.util.Comparator;

public class ItemComperatorGreedy implements Comparator<KnapSackItem>{

	@Override
	public int compare(KnapSackItem arg0, KnapSackItem arg1) {
		return (arg0.getValue() / arg0.getWeight() > arg1.getValue() / arg1.getWeight())?-1:1;
	}

}
