package Task1;

import java.util.ArrayList;
import java.util.List;

public class Bag {
	private List<KnapSackItem> bagItems = new ArrayList<KnapSackItem>();
	private int size;
	
	public Bag() {
		size = 0;
	}
	
	public int getWeight() {
		int w = 0;
		for(KnapSackItem i : bagItems){
			w += i.getWeight();
		}
		return w;
	}
	
	public int getValue() {
		int v = 0;
		for(KnapSackItem i : bagItems){
			v += i.getValue();
		}
		return v;
	}
	
	public void clear() {
		size = 0;
		bagItems.clear();
	}
	
	public Bag addItem(KnapSackItem i) {
		bagItems.add(i);
		size++;
		return this;
	}
	
	public List<KnapSackItem> getItems() {
		return bagItems;
	}
	
	public KnapSackItem removeItem(int pos) {
		if(bagItems.get(pos) != null){
			size--;
		}
		return bagItems.remove(pos);
	}
	
	public void fillBag(List<KnapSackItem> list, int maxWeight) {
		int currentWeight = this.getWeight();
		for (KnapSackItem i : list) {
			if (currentWeight + i.getWeight() <= maxWeight) {
				this.addItem(i);
				size++;
				currentWeight += i.getWeight();
			}
			if (currentWeight == maxWeight)
				break;
		}
	}
	
	public Bag copyBagItems(Bag b) {
		for(KnapSackItem i : b.getItems()){
			this.addItem(i);
		}
		return this;
	}
	
	public boolean contains(KnapSackItem i){
		return bagItems.contains(i);
	}
	
	public int getSize() {
		return size;
	}
}