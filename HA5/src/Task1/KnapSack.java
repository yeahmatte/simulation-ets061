package Task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class KnapSack {
	Random rndGen;
	private int numberOfItems;
	private int weightLimit;
	private List<KnapSackItem> items;
	private List<KnapSackItem> unusableItems;
	public KnapSack(Scanner scan){
		rndGen = new Random();
		
		//Skip past the comments
		while(scan.hasNext()){
			String nextLine = scan.nextLine();
			if(nextLine.charAt(0) != '#'){
				numberOfItems = Integer.valueOf(nextLine); 
				break;
			}
		}

		items = new ArrayList<KnapSackItem>();

		//Process item values
		for(int i = 0; i < numberOfItems; i++){
			items.add(new KnapSackItem("Item "+i, Integer.valueOf(scan.nextLine())));
		}


		//Process item weights 
		for(KnapSackItem item : items){
			item.setWeight(Integer.valueOf(scan.nextLine()));
		}

		weightLimit = Integer.valueOf(scan.nextLine());

		//Process item weights 
		for(KnapSackItem item : items){
			if(item.getWeight() > weightLimit){
				items.remove(item);
				unusableItems.add(item);
			}
		}

	}

	public Bag findKnapSack(int kmax, double alpha, double temperature) {
		Bag currentBag = new Bag();
		String caseSelection = "";
		
		List<KnapSackItem> unusedItems = new ArrayList<KnapSackItem>();
		unusedItems.addAll(items);

		KnapSackItem first = unusedItems.remove(rndNbrInt(0,unusedItems.size()));
		currentBag.addItem(first);
		
		
		int k = 0;
		while( k < kmax ) {
			double rnd = Math.random();
			Bag newBag = new Bag().copyBagItems(currentBag);
			
			if(rnd < 0.333) {
				caseSelection = "Case1";
				if(newBag.getSize()>=1)
					unusedItems.add(newBag.removeItem(rndNbrInt(0,newBag.getSize())));
				
			} else if( rnd < 0.6666) {
				caseSelection = "Case2";
				KnapSackItem rndItem = unusedItems.get(rndNbrInt(0,unusedItems.size()));
				if(!newBag.contains(rndItem) && rndItem.getWeight() <= (weightLimit - newBag.getWeight())){
					newBag.addItem(rndItem);
					unusedItems.remove(rndItem);
				}
				
			} else if( newBag.getSize() >= 1) {
				caseSelection = "Case3";;
				unusedItems.add(newBag.removeItem(rndNbrInt(0,newBag.getSize())));
				KnapSackItem rndItem = unusedItems.get(rndNbrInt(0,unusedItems.size()));
				
				if(!newBag.contains(rndItem) && rndItem.getWeight() <= (weightLimit - newBag.getWeight())) {
					newBag.addItem(rndItem);
					unusedItems.remove(rndItem);
				}
			}

			int deltaValue = newBag.getValue() - currentBag.getValue();
			double val = Math.exp( (newBag.getValue()/currentBag.getValue())/temperature );

			if (deltaValue > 0) {
				currentBag.clear();
				currentBag.copyBagItems(newBag);
				newBag.clear();
			} else if (Math.random() > val) {
				currentBag.clear();
				currentBag.copyBagItems(newBag);
				newBag.clear();
			}
			
			temperature *= alpha;
			k++;
		}

		return currentBag;
	}


	public Bag findKnapSackGreedy(int kmax, double alpha, double temperature) {
		List<KnapSackItem> sortedItems = new ArrayList<KnapSackItem>();
		sortedItems.addAll(items);
		sortedItems.sort(new ItemComperatorGreedy());
		int currentBagValue;
		int newBagValue;

		Bag currentBag = new Bag();
		currentBag.fillBag(sortedItems, weightLimit);

		int nbrOfIterations = kmax;
		int k = 0;
		while (k < nbrOfIterations) {
			int indexToRemove = (int) Math.random() * currentBag.getItems().size();
			Bag newBag = new Bag().copyBagItems(currentBag);
			newBag.removeItem(indexToRemove);
			List<KnapSackItem> tempList = new ArrayList<KnapSackItem>();
			tempList.addAll(sortedItems);
			tempList.removeAll(currentBag.getItems());
			newBag.fillBag(tempList, weightLimit);

			//Test assignments
			currentBagValue = currentBag.getValue();
			newBagValue = newBag.getValue();
			if (k == 50000) { 
				int expr = 1;
			}

			int deltaValue = newBag.getValue() - currentBag.getValue();
			double val = Math.exp( (newBagValue/currentBagValue)/temperature );
			if (deltaValue > 0) {
				currentBag = newBag;
			} else if (Math.random() > val) {
				currentBag = newBag;
			}
			k++;
			temperature *= alpha;
		}
		return currentBag;
	}

	public Bag startBruteforce() {
		return findKnapSackBruteForce(new Bag(), weightLimit, 0);
	}

	private Bag findKnapSackBruteForce(Bag bag, int limit, int nextIndex) {

		if(nextIndex == (numberOfItems-1)) {
			if(limit >= items.get(nextIndex).getWeight()){
				bag.addItem(items.get(nextIndex));
			}
			return bag;
		} else if (limit == 0) {
			return bag;

		} else {
			if(limit >= items.get(nextIndex).getWeight()) {
				Bag bagWith = this.findKnapSackBruteForce(new Bag().copyBagItems(bag).addItem(items.get(nextIndex)), limit-(items.get(nextIndex).getWeight()),nextIndex+1 );
				Bag bagWithout = this.findKnapSackBruteForce(new Bag().copyBagItems(bag),limit,nextIndex+1);

				if(bagWith.getValue() > bagWithout.getValue())
					return bagWith;
				else
					return bagWithout;
			}else{
				return this.findKnapSackBruteForce(new Bag().copyBagItems(bag),limit,nextIndex+1);
			}
		}

	}

	public void printKnapSackList(List<KnapSackItem> l) {
		for(KnapSackItem i : l)
			System.out.println(i.getName() + " V:"+i.getValue() + " W:"+i.getWeight());
	}


	private int rndNbrInt(int lower, int upper) {
		return rndGen.nextInt(upper);
	}

}
