package Task1;

public class KnapSackItem implements Comparable<KnapSackItem> {
	int weight;
	int value;
	String name;
	
	public KnapSackItem(String name, int value, int weight) {
		this.name = name;
		this.weight = weight;
		this.value = value;
	}
	
	public KnapSackItem(String name, int value) {
		this(name,value,0);
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getName()  {
		return name;
	}

	@Override
	public int compareTo(KnapSackItem arg0) {
		// TODO Auto-generated method stub
		return (this.name.compareTo(arg0.getName()) < 0)? -1 : 1;
	}
	
	public boolean equals(KnapSackItem arg0) {
		return (this.name.compareTo(arg0.getName()) == 0);
	}
}